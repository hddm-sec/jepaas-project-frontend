/*
 * @Descripttion:
 * @Author: 张明尧
 * @Date: 2021-02-02 14:37:21
 * @LastEditTime: 2021-02-02 15:59:31
 */
import router from './router';
import config from './config.json';
import install from '../../util/install';

install(router, config);
