import {
  fetchMyGroups,
} from '../actions/group';

export default class Group {
  /*
   * 获取我的群组列表
   */
  static async getGroupList(userId) {
    const res = await fetchMyGroups({
      userId,
    });
    if (res.success) {
      return res.obj || [];
    }
    JE.msg(res.message);
    return [];
  }
}