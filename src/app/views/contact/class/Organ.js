/*
 * @Author: Qinyonglian
 * @Date: 2019-07-23 14:12:50
 * @LastEditors  : qinyonglian
 * @LastEditTime : 2020-02-14 20:45:40
 */


import {
  fetchGetUserInfoById,
  fetchGetUserByOrgId,
  fetchGetOrgById,
  fetchGetOrg,
  fetchGetAllOrg,
  fetchGetAllPeople,
} from '../actions/organ';

export default class Organ {
  // 所有租户下的人员
  static zhList = [];

  static originList = [];

  /*
   * 获取指定用户信息
   * @param {*} userId userId
   */
  static async getUserInfoByUserId(userId) {
    const res = await fetchGetAllOrg({
      userId,
    });
    if (res.success) {
      Organ.originList = res.obj || [];
      return Organ.originList;
    }
    JE.msg(res.message);

    return {};
  }

  /*
   * 获取租户下所有人员的列表（包括总数）
   * @param {*} tenantId
   */
  static async fetchGetAllPeoples(tenantId) {
    JE.showWaiting('数据正在拼命加载中...');
    Organ.zhList = [];
    const res = await fetchGetAllPeople({
      tenantId,
    });
    if (res.success) {
      (res.obj.rows || []).map((item) => {
        const keyArr = ['companyemail', 'createtime', 'deptname', 'easyname', 'gender', 'otheremail', 'phone', 'photo', 'rolenames', 'sentrynames', 'usercode', 'userid', 'username', 'zsldname', 'zuoji'];
        const newItem = {};
        keyArr.map((index) => {
          for (const key in item.value) {
            const keys = key.toLowerCase();
            if (keys === index) {
              newItem[index] = item.value[key];
            }
          }
        });
        newItem.char = JE.toPinYin(newItem.username, 'pinyin').substr(0, 1).toUpperCase();
        const photo = JE.getUserPhoto(newItem.userid, true, newItem.username);
        newItem.photo = photo.replace('src', 'data-src');
        newItem.id = newItem.userid;
        newItem.name = newItem.username;

        Organ.zhList.push(newItem);
      });
      return Organ.zhList;
    }
    JE.msg(res.message);
    return {};
  }

  /*
   * 获取指定用户信息
   * @param {*} userId userId
   */
  static async getUserInfoById(userId) {
    const res = await fetchGetUserInfoById({
      userId,
    });
    if (res.success) {
      return res.obj || {};
    }
    JE.msg(res.message);

    return {};
  }

  /*
   * 根据部门id获取改部门下所有人员信息（包含子部门）
   * @param {*} deptId 部门id
   * @param {*} userId 登录用户id
   */
  static async getUserListByDeptId(deptId, userId) {
    const res = await fetchGetUserByOrgId({
      orgId: deptId,
      userId,
    });
    if (res.success) {
      return res.obj || [];
    }
    JE.msg(res.message);

    return [];
  }

  /*
   * 根据部门id,获取部门信息
   * @param {*} deptId 部门id
   * @param {*} userId 登录用户id
   */
  static async getDeptInfoByDeptId(deptId, userId) {
    const res = await fetchGetOrgById({
      orgId: deptId,
      userId,
    });
    if (res.success) {
      return res.obj || {};
    }
    return {};
  }

  /*
   * 根据deptId获取该部门下的部门和员工
   * @param {*} userId 当前登录用户
   * @param {*} searchText 关键字
   * @param {*} deptId 部门id
   */
  static async getChildByDeptId(userId, deptId, searchText,) {
    const params = {
      userId,
    };
    if (deptId) {
      params.id = deptId;
    }
    if (searchText) {
      params.searchText = searchText;
    }
    const res = await fetchGetOrg(params);
    if (res.success) {
      return res.obj || [];
    }
    JE.msg(res.message);

    return [];
  }
}
