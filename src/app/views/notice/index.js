import Vue from 'vue';
import install from '../../util/install';
import router from './router.js';
import Icon from '@/components/icon';
import config from './config.json';

Vue.component('Icon', Icon);
install(router, config);
