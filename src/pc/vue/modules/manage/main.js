import Vue from 'vue';
// import ElementUI from 'element-ui';
import Manage from './index.vue';
// import 'element-ui/lib/theme-chalk/index.css';
// import from 'element-ui/lib/locale/lang/zh-CN' // lang i18n

// Vue.use(ElementUI, { locale })
Vue.config.productionTip = false;

new Vue({
  el: '#app',
  render: h => h(Manage),
});
