const m = {
  more: '更多',
  suredelete: '确定删除吗',
  create: '创建',
  today: '今天',
  retract: '收起',
  download: '下载',
  confirm: '确定',
  cancel: '取消',
  remind: '提醒',
  save: '保存',
  selectreviewers: '选择点评人',
  close: '关闭',
  close1: '关 闭',
};

export default m;
