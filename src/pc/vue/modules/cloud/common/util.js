/*
 * 全选与全不选数据
 * @param {list} 目标数据
 * @param {checkStatus} true 全选      false非全选
 * @param {attr} 列表中选中的属性
 * @return:
 */
export function getCheckArr(checkStatus, list, attr) {
  if (checkStatus) {
    return list.map((check) => {
      check[attr] = checkStatus;
      return check;
    });
  }
  return list.map((check) => {
    check[attr] = checkStatus;
    return check;
  });
}

/*
 * 全选与全不选数据(公司文件)
 * @param {list} 目标数据
 * @param {checkStatus} true 全选      false非全选
 * @param {attr} 列表中选中的属性
 * @return:
 */
export function getCheckArrCompany(checkStatus, list, attr) {
  if (checkStatus) {
    return list.map((check) => {
      check.roleRoleCode != 'check' && (check[attr] = checkStatus) || (check[attr] = !checkStatus);
      return check;
    });
  }
  return list.map((check) => {
    check[attr] = checkStatus;
    return check;
  });
}

/**
 * 根据参数和对应的要求进行格式判断
 *
 * @export
 * @param {Every} data 需要进行格式判断的数据
 * @param {String} key 需要判断的类型 Array,String,Object等等
 */
export function isTypeData(data, key) {
  const isType = type => obj => Object.prototype.toString.call(obj) === `[object ${type}]`;
  const isTypeArr = isType(key); // 判断是否是数组
  return isTypeArr(data);
}

/**
 * 选中文件之后点击二级菜单的功能进行拦截过滤，判断权限是否统一都满足功能的权限
 * @export
 * @param { Object } title  当前二级菜单的数据和参数
 * @param { Array } checkNow 当前选中的文件数据
 * @param { Object } companyData 当前面包屑的数据
 */
export function menuClickCompany(title, checkNow, companyData) {
  // fileData
  // 先过滤是否是新建或者上传，根据文件权限和平台管理员进行判断(只有这两种情况)
  // if (['add', 'upload'].includes(title.dialogText)) {
  //   const roleCode = fileData.roleRoleCode;

  // }
  const noRoleObj = {
    status: 'role',
    roleTitle: title.text, // 不满足的权限名称
    data: [], // 不满足权限的文件数据
  }; // 将不满足的数据进行处理
  // 如果是数组的话执行该方法
  if (checkNow.length) {
    isTypeData(checkNow, 'Array') && checkNow.forEach((check) => {
      !title.role.includes(check.roleRoleCode) && noRoleObj.data.push(check) && (noRoleObj.status = 'noRole');
    });
    !isTypeData(checkNow, 'Array') && !title.role.includes(checkNow.roleRoleCode) && noRoleObj.data.push(checkNow) && (noRoleObj.status = 'noRole');
  }
  return noRoleObj;
}

/*
 * 非全选数据
 * @param {checkList} 目前选中的数据
 * @param {item} 当前点击的item
 * @param {checkAttr} 列表中选中的属性
 * @param {checkId} 列表中选中的ID唯一标识
 * @return:
 */
export function getNoAllCheckList(item, checkList, checkAttr) {
  if (item[checkAttr]) {
    checkList.push(item);
    return checkList;
  }
  const index_ = checkList.findIndex(mapList => mapList.nodeId == item.nodeId);
  index_ > -1 && checkList.splice(index_, 1);
  return checkList;
}

/*
 * 非全选数据(公司文件过滤)
 * @param {checkList} 目前选中的数据
 * @param {item} 当前点击的item
 * @param {checkAttr} 列表中选中的属性
 * @param {checkId} 列表中选中的ID唯一标识
 * @return:
 */
export function getCompanyCheckList(item, checkList, checkAttr, checkId, mainList) {
  const checkObj = {
    checkList,
    checkAll: false,
  };
  if (item[checkAttr]) {
    checkObj.checkList.push(item);
  } else {
    const index_ = checkObj.checkList.findIndex(mapList => mapList.nodeId == item.nodeId);
    index_ > -1 && checkList.splice(index_, 1);
  }
  const mainListLength = mainList.filter(main => main.roleRoleCode != 'check');
  checkObj.checkAll = (mainListLength.length == checkObj.checkList.length);
  return checkObj;
}

/**
 * 根据权限修改每条数据的ICON标签的显示
 *
 * @export
 * @param { 权限显示 } role
 * @returns
 */
export function companyIconRole(role) {
  let roleIcon = [{ icon: 'jeicon-automatic-pen', clickText: 'edit', name: '重命名' },
    { icon: 'jeicon-download-o', clickText: 'downLoad', name: '下载' },
    { icon: 'jeicon-the-share', clickText: 'JE_User', name: '分享' },
    { icon: 'jeicon-more', clickText: null, name: '更多' },
    { icon: 'jeicon-label', clickText: null, name: '标记' }];
  role && roleIcon.splice(2, 1);
  role == 'download' && (roleIcon = [{ icon: 'jeicon-download-o', clickText: 'downLoad', name: '下载' }]);
  role == 'check' && (roleIcon = []);
  return roleIcon;
}

/**
 * 公司文件操作的显示过滤
 *
 * @export
 * @param { String } role 当前拥有的权限
 * @param { String } diskType 当前是公司文件还是个人文件
 * @returns
 */
export function operationRole(role, diskType, option) {
  let roleIcon = {
    text: [{ title: '设置权限', status: 'admin' }, { title: '操作记录', status: 'record' }],
    icon: null,
  };
  option.fileSuffix && (roleIcon = { text: [{ title: '-', status: null }], icon: null });
  role == 'edit' && !option.fileSuffix && (roleIcon = { text: [{ title: '操作记录', status: 'record' }], icon: null });
  role == 'download' && (roleIcon = { text: [{ title: '-', status: null }], icon: null });
  role == 'check' && (roleIcon = { text: [{ title: '-', status: null }], icon: null });
  diskType != 'company' && (roleIcon = { text: [{ title: '-', status: null }], icon: null });
  return roleIcon;
}

// 根据尾缀来确定icon
export function fileSuffixIcon(suffix) {
  //  先将尾缀统一变成小写
  const unKnow = {
    icon: 'jeicon-unknownfile',
    color: '#A9ABA9',
  };
  const allIcons = [
    {
      icon: 'jeicon-square-flash',
      suffixs: ['swf', 'rm', 'mov', 'swf', 'fla', 'flash'],
      color: '#46DDA2',
    }, // flash 的文件
    {
      icon: 'jeicon-jepg',
      suffixs: ['jpeg'],
      color: '#A3DB73',
      isImg: true, // 是否是图片
    }, // jepg 的文件
    {
      icon: 'jeicon-js',
      suffixs: ['js', 'json', 'script', 'javascript'],
      color: '#F4CE4B',
    }, // js 文件
    {
      icon: 'jeicon-ie',
      suffixs: ['ie', 'IE'],
      color: '#F8A646',
    }, // ie 的文件
    {
      icon: 'jeicon-svg',
      suffixs: ['svg'],
      isImg: true, // 是否是图片
      color: 'rgb(255, 119, 68)',
    }, // svg 的文件
    {
      icon: 'jeicon-square-gif',
      suffixs: ['gif'],
      isImg: true, // 是否是图片
      color: '#A3DB73',
    }, // gif 的文件
    {
      icon: 'jeicon-audio',
      suffixs: ['mp4', 'mp3'],
      color: '#F65A59',
    }, // 音频文件
    {
      icon: 'jeicon-mp3',
      suffixs: ['mp3', 'wma', 'wav', 'ape', 'mid'],
      color: '#F65A59',
    }, // mp3文件
    {
      icon: 'jeicon-exe',
      suffixs: ['exe'],
      color: '#4BB8F3',
    }, // exe文件
    {
      icon: 'jeicon-rar',
      suffixs: ['rar'],
      color: '#FBAF32',
    }, // rar文件
    {
      icon: 'jeicon-psd',
      suffixs: ['psd'],
      color: '#4BB8F3',
    }, // psd文件
    {
      icon: 'jeicon-bmp',
      suffixs: ['bmp'],
      isImg: true, // 是否是图片
      color: '#D088E1',
    }, // bmp文件
    {
      icon: 'jeicon-png',
      suffixs: ['png'],
      isImg: true, // 是否是图片
      color: '#4BB8F3',
    }, // png文件
    {
      icon: 'jeicon-jpg',
      suffixs: ['jpg'],
      isImg: true, // 是否是图片
      color: 'rgb(255, 119, 68)',
    }, // jpg文件
    {
      icon: 'jeicon-html',
      suffixs: ['html'],
      color: '#6DCC51',
    }, // html文件
    {
      icon: 'jeicon-dwf',
      suffixs: ['dwf'],
      color: '#6DCC51',
    }, // dwf文件
    {
      icon: 'jeicon-zip',
      suffixs: ['zip', '7-zip', '7z', 'winzip'],
      color: '#FBAF32',
    }, // zip文件
    {
      icon: 'jeicon-pdf',
      suffixs: ['pdf'],
      color: '#E52C0E',
    }, // pdf文件
    {
      icon: 'jeicon-ppt',
      suffixs: ['pptx', 'pptm', 'ppt', 'potx', 'potm', 'pot', 'ppsx', 'ppsm', 'xml'],
      color: '#F34E19',
    }, // ppt文件
    {
      icon: 'jeicon-txt',
      suffixs: ['txt'],
      color: '#2BA244',
    }, // ppt文件
    {
      icon: 'jeicon-excel',
      suffixs: ['xls', 'xlsx', 'xlsm'],
      color: '#00843E',
    }, // ppt文件
    {
      icon: 'jeicon-word',
      suffixs: ['doc', 'docx', 'dot', 'dotx', 'docm'],
      color: '#0057AA',
    }, // ppt文件
  ];
  if (!suffix) {
    return {
      icon: 'jeicon-folders',
      color: '#F7C12D',
    };
  }
  const lowSuffix = suffix.toLowerCase();
  let suffixObj = [];
  suffixObj = allIcons.filter((suff) => {
    if (suff.suffixs.includes(lowSuffix)) {
      return suff;
    }
  });
  suffixObj.length == 0 && (suffixObj.push(unKnow));
  return suffixObj[0];
}


/**
 * 获取到公司权限接口之后的参数格式处理
 *
 * @export
 * @param { Array } company 获取到的接口参数进行处理
 * @param { String } key 需要获取的每个key的字段
 */
export function roleCompanyData(company, key) {
  const keyName = {
    staff: '人员',
    role: '角色',
    dept: '部门',
    branch: '分公司',
    company: '全公司',
  };
  return company.filter((main) => {
    if (main.role == key) {
      main.shortname = keyName[main.type];
      main.check = true; // 默认人员是选中状态
      main.id = main.code;
      return main;
    }
  });
}


/**
 * 设置权限下拉菜单的标题和内容范围显示
 *
 * @export
 * @param { Array } data 当前选中的人员
 * @param { String } key 当前的权限Key字段
 * @returns
 */
export function roleTitleScope(data, key) {
  const companyList = [];
  const branchList = [];
  const deptList = [];
  const roleList = [];
  const staffList = [];
  data.forEach((item) => {
    if (item.check && item.role == key) {
      if (item.type == 'company') {
        companyList.push(item);
      }
      if (item.type == 'branch') {
        branchList.push(item);
      }
      if (item.type == 'dept') {
        deptList.push(item);
      }
      if (item.type == 'role') {
        roleList.push(item);
      }
      if (item.type == 'staff') {
        staffList.push(item);
      }
    }
  });
  const companyTitle = companyList.length ? `${companyList.length}个公司、` : '';
  const branchTitle = branchList.length ? `${branchList.length}个分公司、` : '';
  const deptTitle = deptList.length ? `${deptList.length}个部门、` : '';
  const roleTitle = roleList.length ? `${roleList.length}个角色、` : '';
  const staffTitle = staffList.length ? `${staffList.length}个人员` : '';
  return `${companyTitle}${branchTitle}${deptTitle}${roleTitle}${staffTitle}`;
}

/**
 * 根据指定的key值,循环处理出相应的数据
 * @export
 * @param {String} key 需要循环出来的key值
 * @param {Array} list 从哪个数组中进行循环
 */
export function getArrStyle(key, list) {
  if (!key || !list) {
    JE.msg('key值或者数组不存在!');
  }
  const newList = []; // 需要返回的整理数据
  list.forEach((val) => {
    newList.push(val[key]);
  });
  return newList;
}

/**
 * 文件下载的公用方法
 *
 * @export
 * @param {下载的地址} url
 * @returns
 */
export function downLoadScript(url) {
  const tempLink = document.createElement('a');
  tempLink.style.display = 'none';
  // tempLink.download = "";
  tempLink.href = url;
  // tempLink.setAttribute('download', filename);

  // Safari thinks _blank anchor are pop ups. We only want to set _blank
  // target if the browser does not support the HTML5 download attribute.
  // This allows you to download files in desktop safari if pop up blocking
  // is enabled.
  // if (typeof tempLink.download === 'undefined') {
  //   tempLink.setAttribute('target', '_blank');
  // }
  document.body.appendChild(tempLink);
  tempLink.click();
  document.body.removeChild(tempLink);
  return false;
}

/**
 *  计算文件大小函数(保留两位小数),Size为字节大小
 *  初始化size
 * @export
 * @param {文件的大小} size
 */
export function getfilesize(size) {
  if (!size) { return ''; }
  const num = 1024.00; // byte
  if (size < num) { return `${size}B`; }
  // eslint-disable-next-line no-restricted-properties
  if (size < Math.pow(num, 2)) { return `${(size / num).toFixed(2)}K`; } // kb
  // eslint-disable-next-line no-restricted-properties
  if (size < Math.pow(num, 3)) { return `${(size / Math.pow(num, 2)).toFixed(2)}M`; } // M
  // eslint-disable-next-line no-restricted-properties
  if (size < Math.pow(num, 4)) { return `${(size / Math.pow(num, 3)).toFixed(2)}G`; } // G
  // eslint-disable-next-line no-restricted-properties
  return `${(size / Math.pow(num, 4)).toFixed(2)}T`; // T
  // Math.pow(x,y) //返回 x 的 y 次幂的值
  // NumberObject.toFixed(num) //可把 Number 四舍五入为指定小数位数的数字
}

/**
 * 根据选中的字段去除掉数组中条件相同的部分
 *
 * @export
 * @param {String} key 需要判断的key值
 * @param {Object} data 需要判断的指定参数
 * @param {Array} list 需要遍历的数组
 * @param {Boolean} isDataKey 判断传递过来的数据是否需要key值，不需要的话直接获取, true: 需要key值; false: 不需要key值
 */
export function getAssignKeyData(key, data, list, isDataKey) {
  // 判难当前data的数据格式
  const isType = type => obj => Object.prototype.toString.call(obj) === `[object ${type}]`;
  const isTypeArr = isType('Array'); // 判断是否是数组
  if (!isTypeArr(data)) return JE.msg('只能传递数组才可进行判断');
  let data_ = []; // 处理完之后的数据
  // 获取下标并且是两个数组进行判断
  // 如果不需要key值的判断
  if (!isDataKey) {
    data_ = list.filter(dataIndex => !data.includes(dataIndex[key]));
  }
  // 如果需要key值的判断
  if (isDataKey) {
    data_ = list.filter(item => data.some(ele => ele[key] == item[key]));
  }
  return data_;
}

// 获取系统变量判读是否是文档的最高级别管理员
export function isSysAdmin() {
  // const nowUser = JE.getCurrentUser().id;
  // const userIds = JE.systemConfig.JE_SYS_DOCADMINID.split(',') || [];
  // return userIds.includes(nowUser);
  return JSON.parse(sessionStorage.getItem('fileAdmin'));
}

// 公司文件和个人文件
function companyAndOwnDoc(router, routerText) {
  let adminSys = false;
  router == '1-2' && (adminSys = true) || (adminSys = isSysAdmin());
  const menuMap = {
    headMenu: [
      {
        title: '全部文件',
        icon: null,
        children: null,
        showEle: true,
        change: true,
      },
      {
        title: '文档',
        icon: null,
        showEle: false,
        children: [
          {
            title: 'Word',
            type: ['doc', 'docx', 'dot', 'dotx', 'docm'],
            time: null,
          },
          {
            title: 'Excel',
            type: ['xls', 'xlsx', 'xlsm'],
            time: null,
          },
          {
            title: 'PPT',
            type: ['pptx', 'pptm', 'ppt', 'potx', 'potm', 'pot', 'ppsx', 'ppsm', 'xml'],
            time: null,
          },
          {
            title: 'PDF',
            type: ['pdf'],
            time: null,
          },
        ],
      },
      {
        title: '图片',
        icon: null,
        showEle: false,
        children: [
          {
            title: 'JPG',
            type: ['jpg'],
            time: null,
          },
          {
            title: 'PNG',
            type: ['png'],
            time: null,
          },
          {
            title: 'GIF',
            type: ['gif'],
            time: null,
          },
          {
            title: 'SVG',
            type: ['svg'],
            time: null,
          },
          {
            title: 'JPEG',
            type: ['jpeg'],
            time: null,
          },
        ],
      },
      {
        title: '视频',
        icon: null,
        showEle: false,
        children: [
          {
            title: 'MV',
            type: ['rmvb', 'mv', 'avi', 'mtv', 'rm', '3gp', 'amv', 'flv', 'dmv', 'mp4', 'mpeg1', 'mpeg2', 'mpeg4'],
            time: null,
          },
          {
            title: 'Flash',
            type: ['swf', 'rm', 'mov', 'swf', 'fla'],
            time: null,
          },
        ],
      },
      {
        title: '音频',
        icon: null,
        showEle: false,
        children: [
          {
            title: 'MP3',
            type: ['mp3', 'wma', 'wav', 'ape', 'mid', 'MP3'],
            time: null,
          },
          {
            title: 'MP4',
            type: ['mp4', 'MP4'],
            time: null,
          },
        ],
      },
      {
        title: '压缩文件',
        icon: null,
        showEle: false,
        children: [
          {
            title: 'ZIP',
            type: ['zip', '7-zip', '7z', 'winzip'],
            time: null,
          },
          {
            title: 'RAR',
            type: ['rar', 'RAR'],
            time: null,
          },
          {
            title: 'CAB',
            type: ['CAB', 'cab'],
            time: null,
          },
          {
            title: 'TAR',
            type: ['TAR', 'tar'],
            time: null,
          },
        ],
      },
    ], // 头部菜单数据
    head: true, // 全部文件是否显示
    rectangleIcon: 'jeicon-product-list', // 当前显示的排列方式（矩形还是列表）icon
    router,
    admin: adminSys, // 判断当前的用户是否是系统超级管理员
    routerText, // 方便新建文件的功能
    headStatus: '1', // 全部文件的第一种样式
    menuData: router == '1-1' ? [
      {
        text: '新建文件夹',
        icon: 'jeicon-plus',
        dialogText: 'add', // 弹出框的标记
        status: adminSys,
        role: ['manage'], // 公司文件的权限设置
      },
      {
        text: '上传',
        icon: 'jeicon-upload-o',
        dialogText: 'upload', // 弹出框的标记
        status: adminSys,
        role: ['manage'],
      },
      {
        text: '下载',
        icon: 'jeicon-download-o',
        dialogText: 'downLoad', // 弹出框的标记
        status: false,
        role: ['manage', 'edit', 'download'],
      },
      {
        text: '复制',
        icon: 'jeicon-copy',
        dialogText: 'copy', // 弹出框的标记
        status: false,
        role: ['manage', 'edit'],
      },
      {
        text: '移动到',
        icon: 'jeicon-mobile-o',
        dialogText: 'move', // 弹出框的标记
        status: false,
        role: ['manage', 'edit'],
      },
      {
        text: '至微邮',
        icon: 'jeicon-envelope-o',
        dialogText: 'mail',
        status: false,
        role: ['manage', 'edit'],
      },
      {
        text: '删除',
        icon: 'jeicon-trash-o',
        dialogText: 'remove', // 弹出框的标记(删除)
        status: false,
        role: ['manage', 'edit'],
      },
    ] : [
      {
        text: '新建文件夹',
        icon: 'jeicon-plus',
        dialogText: 'add', // 弹出框的标记
        status: adminSys,
      },
      {
        text: '上传',
        icon: 'jeicon-upload-o',
        dialogText: 'upload', // 弹出框的标记
        status: adminSys,
      },
      {
        text: '下载',
        icon: 'jeicon-download-o',
        dialogText: 'downLoad', // 弹出框的标记
        status: false,
      },
      {
        text: '分享',
        icon: 'jeicon-the-share',
        dialogText: 'JE_User', // 弹出框的标记(调用JE的人员选择)
        status: false,
      },
      {
        text: '复制',
        icon: 'jeicon-copy',
        dialogText: 'copy', // 弹出框的标记
        status: false,
      },
      {
        text: '移动到',
        icon: 'jeicon-mobile-o',
        dialogText: 'move', // 弹出框的标记
        status: false,
      },
      {
        text: '至微邮',
        icon: 'jeicon-envelope-o',
        dialogText: 'mail',
        status: false,
      },
      {
        text: '删除',
        icon: 'jeicon-trash-o',
        dialogText: 'remove', // 弹出框的标记(删除)
        status: false,
      },
    ], // 新建 上传等菜单
    hidenSearch: false, // 是否隐藏搜索功能
    secondHead: true, // 头部的二级菜单是否显示
    secondHeadStatus: '1', // 头部的二级菜单的第一种样式
  };
  return menuMap;
}
// 我发出的分享和我收到的分享
function emitAndReceive(router) {
  const menuMap = {
    headMenu: [
      {
        title: '清除全部',
        icon: 'jeicon-trash-o',
        children: null,
        showEle: true,
      },
    ],
    router,
    rectangleIcon: 'jeicon-product-list', // 当前显示的排列方式（矩形还是列表）icon
    hidenSearch: false, // 是否隐藏搜索功能
    head: true, // 全部文件是否显示
    headStatus: '2', // 全部文件的第二种样式
    menuData: [
      {
        text: '删除',
        icon: 'jeicon-trash-o',
        dialogText: 'remove', // 弹出框的标记(删除)
        status: false,
      },
    ], // 新建 上传等菜单
    secondHead: true, // 头部的二级菜单是否显示
    secondHeadStatus: '2', // 头部的二级菜单的第二种样式
    listTitle: [
      {
        text: '文件名',
        icon: true,
        span: 9,
        type: 'name',
        name: true,
        size: true,
      },
      {
        text: '大小',
        icon: true,
        span: 4,
        type: 'size',
        name: true,
        size: true,
      },
      {
        text: '创建人',
        icon: false,
        span: 4,
        type: null,
      },
      {
        text: '修改时间',
        icon: false,
        span: 4,
        type: null,
      },
      {
        text: '操作',
        icon: false,
        span: 3,
        type: null,
      },
    ],
  };
  return menuMap;
}
// 正在上传功能和传输记录
function uploading(router) {
  const menuMap = {
    head: true, // 全部文件是否显示
    headMenu: [
      {
        title: '清除全部',
        icon: 'jeicon-trash-o',
        children: null,
        showEle: true,
        dialogText: 'remove', // 弹出框的标记(删除)
      },
    ],
    menuData: [
      {
        text: '删除',
        icon: 'jeicon-trash-o',
        dialogText: 'remove', // 弹出框的标记(删除)
        status: false,
      },
    ],
    router,
    rectangleIcon: 'jeicon-product-list', // 当前显示的排列方式（矩形还是列表）icon
    hidenSearch: true, // 是否隐藏搜索功能
    headStatus: '2', // 全部文件的第二种样式
    secondHead: false, // 头部的二级菜单是否显示
  };
  return menuMap;
}
// 传输记录
function trans(router) {
  const menuMap = {
    head: true, // 全部文件是否显示
    headMenu: [
      {
        title: '清空记录',
        icon: 'jeicon-remove',
        children: null,
        showEle: true,
        dialogText: 'remove_trans', // 弹出框的标记(清空记录)
      },
    ],
    router,
    menuData: [
      {
        text: '删除',
        icon: 'jeicon-trash-o',
        dialogText: 'remove', // 弹出框的标记(删除)
        status: false,
      },
    ],
    rectangleIcon: 'jeicon-product-list', // 当前显示的排列方式（矩形还是列表）icon
    hidenSearch: false, // 是否隐藏搜索功能
    headStatus: '2', // 全部文件的第二种样式
    secondHead: false, // 头部的二级菜单是否显示
  };
  return menuMap;
}
// 回收站
function recycle(router) {
  const menuMap = {
    head: true, // 全部文件是否显示
    headMenu: [
      {
        title: '清空回收站',
        icon: 'jeicon-remove',
        children: null,
        dialogText: 'remove_recycle', // 弹出框的标记(清空回收站)
        showEle: true,
      },
    ],
    menuData: [
      {
        text: '删除',
        icon: 'jeicon-trash-o',
        dialogText: 'remove', // 弹出框的标记(删除)
        status: false,
      },
    ],
    router,
    rectangleIcon: 'jeicon-product-list', // 当前显示的排列方式（矩形还是列表）icon
    hidenSearch: false, // 是否隐藏搜索功能
    headStatus: '2', // 全部文件的第二种样式
    secondHead: false, // 头部的二级菜单是否显示
  };
  return menuMap;
}

// 左侧菜单列表点击之后的拦截处理(修改右侧样式显示)

export function menuSelect(router) {
  let menu = {};
  switch (router) {
    case '1-1':
      // 公司文件
      menu = companyAndOwnDoc(router, 'company');
      break;
    case '1-2':
      // 个人文件
      menu = companyAndOwnDoc(router, 'self');
      break;
    case '1-3':
      // 我发出的分享
      menu = emitAndReceive(router);
      break;
    case '1-4':
      // 我收到的分享
      menu = emitAndReceive(router);
      break;
    case '2':
      // 正在上传
      menu = uploading(router);
      break;
    case '3':
      // 传输记录
      menu = trans(router);
      break;
    case '5':
      // 回收站
      menu = recycle(router);
      break;
  }
  return menu;
}
