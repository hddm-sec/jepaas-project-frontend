/* eslint-disable new-cap */
/*
 * @Descripttion:
 * @version:
 * @Author: qinyonglian
 * @Date: 2019-11-04 14:52:24
 * @LastEditors: Shuangshuang Song
 * @LastEditTime: 2020-07-10 17:02:07
 */
import BaseModel from './baseModel';

export default class acceptModel extends BaseModel {
  constructor(option) {
    super(option);// 相当于获得父类的this指向,继承属性

    this._init(option);
  }


  _init(option) {
    // 如果是自己的私有属性
    this.createUser = option.createUserName; // 创建者
    this.shareStatus = option.shareStatus; // 文件下载状态
    this.transferAddress = option.transferAddress; // 转存后的地址
    this.shareInName = option.shareType != 'receive' && option.shareInName || null; // 我发出分享的人员姓名
    this.id = option.id; // id
    this.shareStatus = option.shareStatus; // 源文件是否删除'delete'就是源文件删除了
    // 选中的文件 或者文件夹有哪些操作功能
    this.docFunc = option.shareType == 'receive' && !option.transferAddress && option.shareStatus != 'delete' ? [
      { icon: 'jeicon-reduction', clickText: 'receive', name: '转存' },
      { icon: 'jeicon-trash-o', clickText: 'remove', name: '删除' },
    ] : [
      { icon: 'jeicon-trash-o', clickText: 'remove', name: '删除' },
    ];
    this.operation = {
      text: '',
      icon: 'jeicon-trash-o',
    };
  }

  /*
   * 创建对象
   */
  static create(options) {
    return new acceptModel(options);
  }
}
