import { install } from '../../install.js';
import index from './index.vue';

// 编辑器
const js = [
    '/static/ux/moment/moment.min.js',
];
JE.loadScript(js);
// 安装组件
install('notice', index);