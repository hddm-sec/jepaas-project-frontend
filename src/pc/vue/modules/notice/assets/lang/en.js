export const m = {
  inform_create: 'create', // 新建微邮
  inform_unknow: 'unknow', // 未知
  inform_nocontent: 'No content', // 无内容
  inform_download: 'Download', // 下载
  inform_preview: 'Preview', // 预览
  inform_newail: 'New micro mail', // 新建微邮
  inform_recipients: 'Recipients', // 收件人
  inform_pleaseschoose: 'Please choose', // 请选择
  inform_setmastersender: 'Set the master sender', // 设置主送人
  inform_theme: 'Theme', // 主题
  inform_content: 'Content', // 正文
  inform_accessory: 'Accessory', // 附件
  inform_emoji: 'Emoji', // 表情
  inform_send: 'Send', // 发 布
  inform_sendcomment: 'Send Comment',
  inform_cancel: 'Cancel',
  inform_fullscreen: 'Fullscreen',
  inform_send1: 'Send', // 发 布
  inform_close: 'Close', // 关 闭
  inform_modification: 'Modification', // 修改
  inform_delete: 'Delete', // 删除
  inform_addresser: 'Addresser', // 发件人
  inform_time: 'Time', // 时间
  inform_like: 'Like', // 点赞
  inform_read: 'Read', // 已阅
  inform_unread: 'Unread', // 未阅
  inform_nouploaded: 'No attachment uploaded', // 没有上传附件
  inform_comments: 'Comments', // 评论
  inform_reply: 'Reply', // 回复
  inform_nosubjectcontent: ' No subject content', // 暂无主题内容
  inform_nocomment: 'No Comment', // 暂无评论内容
  inform_keyword: 'Keyword', // 关键字
  inform_date: 'Date', // 日期
  inform_nametheme: 'Name/Theme', // 姓名/主题
  inform_begingdate: 'Begin', // 开始日期
  inform_enddate: 'End', // 结束日期
  inform_selected: 'Selected', // 已选
  inform_sure: 'Sure', // 确 定
  inform_allremind: 'All remind', // 全部提醒
  inform_requirements: 'Upload format requirements: up to 20 files', // 上传格式要求： 最多支持上传6个文件，大小30M
  inform_publish: 'Release postil',
  inform_postil: 'Postil',
  inform_micromail: 'Micro mail',
  inform_loading: 'loading...',
  inform_clearall: 'A key to empty',
  weiyou_fullscreen: 'Fullscreen',
  weiyou_send1: 'Send', // 发 布
  weiyou_cancel: 'Cancel',
  weiyou_sure: 'Sure',
  weiyou_close: 'Close',
};
