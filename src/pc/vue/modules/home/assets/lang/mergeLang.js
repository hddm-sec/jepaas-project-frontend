import zh from './common/zh.js';
import en from './common/en.js';
import zhAlert from './common/zhAlert';
import enAlert from './common/enAlert';

const commonLang = {
  zh,
  en,
};
const alert = {
  zhAlert,
  enAlert,
};

/**
 *
 * @param {*} lacale 当前语言环境
 * @param {*} mergeList 需要合并的语言包
 */
function getLang(lacale, mergeList) {
  // 设置通用语言包，默认为中文包；
  let finallyLang = commonLang[lacale] || commonLang.zh;
  const alertLang = alert[`${lacale}Alert`] || alert.zhAlert;
  finallyLang = Object.assign({}, finallyLang, { alert: alertLang });
  // 按需加载需要的模块/组件业务语言包,合并通用语言包和业务语言包
  if (mergeList && mergeList instanceof Array) {
    mergeList.forEach((item) => {
      const { name, data } = item;
      const tempObj = {};
      tempObj[name] = data.default;
      finallyLang = Object.assign({}, finallyLang, tempObj);
    });
  }
  return finallyLang;
}

export default getLang;
